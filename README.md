# Midi Sniffer

Este es un proyecto de juguete que preparé para la JSDayCAN2018

Usa webmidi y react para interactuar con dispositivos midi.

La idea es que, una vez seleccionado los dispositivos midi de entrada y salida, se reflejen en la pantalla los eventos que llegan,
y se puedan enviar eventos a los dispositivos desde la web.

Para probarlo está en linea aquí: https://www.graph-ic.org/react-experiments/midi-demos/midisniffer/public/

Para usarlo en local:
- npm run build