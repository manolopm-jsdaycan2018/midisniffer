import React from 'react'
import ReactDOM from 'react-dom'
import Sniffer from './components/sniffer.js'

ReactDOM.render(<Sniffer />, document.getElementById('content'))
